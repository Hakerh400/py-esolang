from util import *

pr_dir = pth_join(cwd, 'private')
pr_file = pth_join(pr_dir, 'key.txt')
pu_dir = pth_join(cwd, 'public')
pu_file = pth_join(pu_dir, 'key.txt')

def main():
  get_and_run_cmd()

def get(s):
  print(s, end=' ')
  inp = input()
  print('')
  return inp

def get_cmd():
  print('1) Write program')
  print('2) Run program')
  print('')
  
  cmd = get('>')
  
  if cmd >= '1' and cmd <= '2':
    return int(cmd)

def sign(buf):
  pem = rfs(pr_file)
  pr = load_pem_private_key(pem, None)
  sig = pr.sign(buf, auth_padding, hashes.SHA1())
  return sig

def verify(sig, buf):
  pem = rfs(pu_file)
  pu = load_pem_public_key(pem)
  
  try:
    pu.verify(sig, buf, auth_padding, hashes.SHA1())
    return 1
  except:
    return 0

def write_prog(pth):
  lines = sanl(rfs(pth, 1).strip())
  
  if lines[-1][0 : 1] == '#':
    lines.pop()
  
  s = '\n'.join(lines)
  hash = sha512(s)
  sig = sign(hash)
  
  s += '\n#{}'.format(hex_enc(sig))
  wfs(pth, s)

def run_prog(pth):
  lines = sanl(rfs(pth, 1).strip())
  
  if lines[-1][0 : 1] != '#':
    print_err('Missing signature')
    return
  
  sig_line = lines.pop()
  sig_hex = sig_line[1:]
  
  def err_sig():
    print_err('Invalid signature')
  
  if len(sig_hex) % 2 != 0:
    return err_sig()
  
  for c in sig_hex:
    if c >= '0' and c <= '9':
      continue
    if c >= 'A' and c <= 'F':
      continue
    return err_sig()
  
  sig = hex_dec(sig_hex)
  s = '\n'.join(lines)
  hash = sha512(s)
  
  if not verify(sig, hash):
    return err_sig()
  
  exec(s)

def cmd_write_prog():
  pth = get('Path to python file:')
  write_prog(pth)

def cmd_run_prog():
  pth = get('Path to py-esolang file:')
  run_prog(pth)

def run_cmd(cmd):
  if cmd == 1:
    return cmd_write_prog()
  
  if cmd == 2:
    return cmd_run_prog()

def get_and_run_cmd():
  cmd = get_cmd()
  
  if cmd is None:
    print_err('Unknown command')
    return
  
  run_cmd(cmd)

main()