import io
import os
import sys
import math
import random
import string
import zipfile
import datetime
import warnings
import traceback
from os import path
from datetime import datetime as dt
from base64 import b64encode, b64decode
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.ciphers import (
  Cipher, algorithms, modes,
)
from cryptography.hazmat.primitives.asymmetric import padding, rsa, dsa
from cryptography.hazmat.primitives.serialization import (
  Encoding, PrivateFormat, PublicFormat, NoEncryption,
  load_pem_private_key, load_pem_public_key,
)

null_byte = b'\0'
usc = '\x5F'
line_sep = os.linesep

cwd = os.getcwd()

auth_padding = padding.PSS(
  mgf=padding.MGF1(hashes.SHA1()),
  salt_length=padding.PSS.MAX_LENGTH,
)

#####

def to_str(x, safe=0):
  t = type(x)
  if t == str:
    return x
  if t == bytes:
    return x.decode('utf8')
  return str(x)

def to_safe_str(x):
  return to_str([x])

def to_buf(x):
  t = type(x)
  if t == bytes:
    return x
  if t == str:
    return x.encode('utf8')
  err(x)

def sanl(s):
  return s.splitlines()

def int_to_hex_digit(n):
  if n <= 9:
    return str(n)
  return string.ascii_uppercase[n - 10]

def hex_digit_to_int(s):
  s = s.upper()
  index = index_of(s, string.digits)
  if index != -1:
    return index
  index = index_of(s, string.ascii_uppercase)
  if index == -1:
    err()
  return index + 10

def byte_to_hex(byte):
  return int_to_hex_digit(byte // 16) +\
    int_to_hex_digit(byte % 16)

def hex_to_byte(s):
  return hex_digit_to_int(s[0]) * 16 +\
    hex_digit_to_int(s[1])

def hex_enc(buf):
  s = ''
  for byte in to_buf(buf):
    s = s + byte_to_hex(byte)
  return s

def hex_dec(s):
  list = []
  for i in range(0, len(to_str(s)), 2):
    list.append(hex_to_byte(s[i : i + 2]))
  return bytes(list)

def sha(alg, buf):
  digest = hashes.Hash(alg)
  digest.update(to_buf(buf))
  return digest.finalize()

def sha1(buf):
  return sha(hashes.SHA1(), buf)

def sha256(buf):
  return sha(hashes.SHA256(), buf)

def sha512(buf):
  return sha(hashes.SHA512(), buf)

def mk_pem_header(type_id, header):
  dashes = '-' * pem_header_dashes_num
  type = type_id == 0 and pem_begin or pem_end
  return ''.join([dashes, type, ' ', header, dashes]).upper()

def pem_to_buf(pem):
  s = ''.join(sanl(pem)[1 : -1])
  return b64_dec(s)

def buf_to_pem(header, buf):
  return line_sep.join([
    mk_pem_header(0, header),
    b64_enc(buf, lines=1),
    mk_pem_header(1, header),
  ])

def pem_get_header(pem):
  s = sanl(pem)[0]
  i = pem_header_dashes_num + len(pem_begin) + 1
  j = -pem_header_dashes_num
  return s[i : j].lower()

def modify_pem(fh, f, pem):
  header = fh(pem_get_header(pem))
  buf = f(pem_to_buf(pem))
  if buf is None:
    return None
  return buf_to_pem(header, buf)

def center_win(win, w=None, h=None, include_header=True):
  if w is None:
    w = win.winfo_width()
  if h is None:
    h = win.winfo_height()
  if include_header:
    w1 = win.winfo_rootx() - win.winfo_x()
    h1 = win.winfo_rooty() - win.winfo_y()
    w = w + 2 * w1
    h = h + h1 + w1
  dx = 448 - 456
  dy = 127 - 157
  x = dx + (win.winfo_screenwidth() - w) // 2
  y = dy + (win.winfo_screenheight() - h) // 2
  win.geometry('{}x{}+{}+{}'.format(w, h, x, y))

def pth_join(pth1, pth2):
  return path.normpath(path.join(pth1, pth2))

def pth_loc(pth):
  return pth_join(cwd, pth)

def rfs(pth, stringify=0):
  with open(pth, 'rb') as f:
    data = f.read()
  if stringify:
    data = to_str(data)
  return data

def wfs(pth, buf):
  with open(pth, 'wb') as f:
    f.write(to_buf(buf))

def index_of(x, xs):
  if x in xs:
    return xs.index(x)
  return -1

def ellipsis(str, n):
  if len(str) <= n:
    return str
  s = ellipsis_str
  return str[0 : max(0, n - len(s))] + s

def show_time(t):
  s1 = ''.join(map(lambda n: '{:02d}.'.format(n), [t.day, t.month, t.year]))
  s2 = ':'.join(map(lambda n: '{:02d}'.format(n), [t.hour, t.minute, t.second]))
  return ' '.join([s1, s2])

def concat(xs):
  xs = list(xs)
  ys = []
  for i in range(0, len(xs)):
    x = xs[i]
    for j in range(0, len(x)):
      ys.append(x[j])
  return ys

def concat_map(f, xs):
  return concat(map(f, list(xs)))

def ite(p, x, y):
  if p:
    return x
  return y

def dite(p, x, y):
  if p:
    return x()
  return y()

def last(xs):
  if len(xs) == 0:
    return None
  return xs[-1]

def buf_to_text(buf, lines=1):
  s = b64_enc(
    buf,
    lines=lines,
    line_len=buf_to_text_line_len,
  )
  return s.replace('+', '-').\
    replace('/', usc).\
    replace('=', '').\
    strip()

def text_to_buf(s):
  for c in s.lower():
    if c >= '0' and c <= '9':
      continue
    if c >= 'a' and c <= 'z':
      continue
    if c == '-' or c == usc:
      continue
    if c == '\r' or c == '\n':
      continue
    return
  s = s.replace('-', '+').\
    replace(usc, '/')
  return b64_dec(s)

def ser_obj(x):
  t = type(x)
  if t == bytes:
    return '`{}'.format(hex_enc(x))
  return str([x])[1 : -1]

def dser_obj(s):
  s = to_str(s)
  if s[0 : 1] == '`':
    return hex_dec(s[1:])
  return eval(s)

def show_mp(mp):
  def f(x):
    (k, v) = x
    return '{}: {}'.format(
      to_str(k),
      ser_obj(v),
    )
  
  xs = list(map(f, mp.items()))
  
  return line_sep.join(xs)

def read_mp(s):
  s = to_str(s)
  mp = {}
  
  for line in sanl(s):
    index = index_of(':', line)
    key = line[0 : index]
    val = line[index + 2 :]
    mp[key] = dser_obj(val)
  
  return mp

def zip(buf):
  buf1 = io.BytesIO()
  
  with zipfile.ZipFile(buf1, 'w', compresslevel=9) as z:
    z.writestr('1', to_buf(buf))
  
  return buf1.getvalue()

def unzip(buf):
  buf1 = io.BytesIO(buf)
  
  with zipfile.ZipFile(buf1, 'r') as z:
    buf = z.read('1')
  
  return buf

def logb():
  print('{}{}{}'.format(line_sep, '=' * 100, line_sep))

def get_stack(trim=0):
  s = sanl(''.join(traceback.format_stack()))
  offset = ite(trim, 2, 0)
  return line_sep.join(map(lambda s: s[offset:], s))

def fail():
  raise Exception()

def err(msg=''):
  logb()
  if msg != '':
    print(msg)
    logb()
  # print(get_stack())
  logb()
  fail()

def xor(buf1, buf2):
  n = len(buf1)
  assert len(buf2) == n
  
  buf = []
  
  for i in range(0, n):
    buf.append(buf1[i] ^ buf2[i])
  
  return bytes(buf)

def nat_to_str_pad(size, n):
  s = str(n)
  return '0' * (64 - len(s)) + s

def checksum(buf, hex=1):
  out = sha1(buf)
  if hex:
    out = hex_enc(out)
  return out

def is_hex_upper_1(c):
  if c >= '0' and c <= '9':
    return 1
  if c >= 'A' and c <= 'F':
    return 1
  return 0

def is_hex_upper(s):
  for c in s:
    if not is_hex_upper_1(c):
      return 0
  return 1

def find_index(f, xs):
  for i in range(0, len(xs)):
    if f(xs[i]):
      return i
  return -1

def find(f, xs):
  for x in xs:
    if f(x):
      return x
  return None

def filter(f, xs):
  ys = []
  for x in xs:
    if f(x):
      ys.append(x)
  return ys

def lf(s):
  return '\n'.join(sanl(to_str(s)))

def get_ctor(obj):
  return obj.__class__

def from_maybe(z, m):
  return ite(m is None, z, m)

def rand(a, b=None):
  if b is None:
    b = a
    a = 0
  return random.randint(a, b - 1)

def rand_bit():
  return rand(2)

def rand_byte():
  return rand(256)

def ca(n, f):
  xs = []
  for i in range(0, n):
    xs.append(f(i))
  return xs

def rand_buf(size):
  return bytes(ca(size, lambda i: rand_byte()))

def max_lines(n, s):
  xs = sanl(to_str(s))
  if len(xs) <= n:
    return s
  xs = sanl(s)[0 : n - 1]
  xs.append(ellipsis_str)
  return line_sep.join(xs)

def nat_to_hex(n):
  return hex(n)[2:].upper()

def nat_to_buf(n):
  h = nat_to_hex(n)
  if len(h) % 2 == 1:
    h = '0' + h
  return hex_dec(h)

def buf_to_nat(buf):
  return int(to_buf(buf).hex(), 16)

def pth_get_base(pth):
  return path.basename(pth)

def pth_get_name(pth):
  return path.splitext(pth_get_base(pth))[0]

def pth_get_ext(pth):
  return path.splitext(pth_get_base(pth))[1][1:]

def get_file_bases_with_ext(dir, ext):
  xs = []
  
  for base in os.listdir(dir):
    pth = pth_join(dir, base)
    if not path.isfile(pth):
      continue
    if pth_get_ext(pth) != ext:
      continue
    xs.append(base)
  
  return xs

def get_file_names_with_ext(dir, ext):
  return [
    pth_get_name(base)
    for base in get_file_bases_with_ext(dir, ext)
  ]

def get_file_pths_with_ext(dir, ext):
  return [
    pth_join(dir, base)
    for base in get_file_bases_with_ext(dir, ext)
  ]

def print_err(msg):
  print('ERROR: {}'.format(msg))